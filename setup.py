from setuptools import setup
from setuptools.command.install import install

package_name = 'flamework'


class InstallWithCompile(install):
    def run(self):
        install.run(self)


setup(
    name=package_name,
    description='Python framework powered by Flask, Mongo and ElasticSearch to handle server-side applications',
    version='0.1',
    packages=['flamework'],
    scripts=[],
    install_requires=[
        'Flask',
        'Flask-Cors',
        'flask_restful',
        'marshmallow',
        'Flask-PyMongo',
        'Pillow',
        'elasticsearch>=6.0.0',
        'redis',
        'python-dateutil',
        'celery'
    ],
    include_package_data=True,
    cmdclass={
        'install': InstallWithCompile,
    },
)
