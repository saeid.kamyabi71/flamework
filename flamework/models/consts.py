class UserRoles:
    USER = 0
    OPERATOR = 1
    ADMIN = 3

    MAP = {
        USER: 'user',
        OPERATOR: 'operator',
        ADMIN: 'admin'
    }
    RMAP = {
        'admin': ADMIN,
        'user': USER,
        'operator': OPERATOR
    }
