from flamework.helpers.event import EventHandler
from flamework.helpers.exceptions import NotFoundError


class BaseLogic(EventHandler):
    __schema__ = None

    def __init__(self, db_handler, es_handler=None):
        self.key = None
        self.db_handler = db_handler
        self.es_handler = es_handler

    def before_upsert(self, _id, properties):
        self.fire_event('before_upsert', _id, properties)

    def after_upsert(self, _id, properties):
        self.fire_event('after_upsert', _id, properties)

    def before_delete(self, _id, item, **kwargs):
        self.fire_event('before_delete', _id, item, **kwargs)

    def after_delete(self, _id, deleted_item):
        self.fire_event('after_delete', _id, deleted_item)

    def before_query(self, filters):
        self.fire_event('before_query', filters)

    def before_index(self, _id, doc):
        self.fire_event('before_index', _id, doc)

    def get_default_fields(self, fields):
        return fields or self.__schema__.get_default_fields() if fields is not None else None

    def get_defaults(self, fields, sort, load_defaults=True):
        if not sort and load_defaults:
            sort = self.__schema__.get_default_sort()
        sorts = self.__schema__.get_sort()
        final_sort = [*sort, *sorts]
        final_sort = sorted(final_sort, key=lambda x: x[0])
        return self.get_default_fields(fields), final_sort

    def search(self, page=1, per_page=30,
               fields=None, sort=None, **filters):
        self.before_query(filters)
        fields, sort = self.get_defaults(fields, sort)
        result, has_next, paginate_result = self.es_handler.paginate(fields=fields, sort=sort,
                                                                     page=page, per_page=per_page,
                                                                     **filters)
        return result, has_next, paginate_result

    def paginate(self, page=1, per_page=30,
                 fields=None, sort=None, **filters):
        self.before_query(filters)
        fields, sort = self.get_defaults(fields, sort)
        result, has_next, paginate_result = self.db_handler.paginate(fields=fields, sort=sort,
                                                                     page=page, per_page=per_page,
                                                                     **filters)
        return result, has_next, paginate_result

    def find_one(self, fields=None, **filters):
        self.before_query(filters)
        fields = self.get_default_fields(fields)
        result = self.db_handler.find_one(fields=fields, **filters)
        return result

    def get_all(self, fields=None, sort=None, **filters):
        self.before_query(filters)
        fields, sort = self.get_defaults(fields, sort)
        result = self.db_handler.get_all(fields=fields, sort=sort, **filters)
        return result

    def get(self, _id, fields=None, **filters):
        if fields is not None:
            fields = self.get_default_fields(fields)
        item = self.db_handler.get(_id, fields=fields, **filters)
        if not item:
            raise NotFoundError()
        return item

    def exists(self, **filters):
        self.before_query(filters)
        return self.db_handler.exists(**filters)

    def new(self, **properties):
        self.before_upsert(None, properties)
        result = self.db_handler.insert(**properties)
        self.index(result['_id'], result)
        self.after_upsert(result['_id'], properties)
        return result

    def update(self, _id, wait=True, **properties):
        self.before_upsert(_id, properties)
        result = self.db_handler.update(_id, **properties)
        if not result:
            raise NotFoundError()
        self.index(_id, wait=wait)
        self.after_upsert(_id, properties)
        return result

    def bulk_upsert(self, items, **kwargs):
        pass

    def delete(self, _id, wait=True, **kwargs):
        item = self.get(_id)
        if not item:
            raise NotFoundError()
        self.before_delete(_id, item, **kwargs)
        result = self.db_handler.delete(_id)
        if not result:
            raise NotFoundError()
        if self.es_handler:
            self.es_handler.delete(_id, wait=wait)
        self.after_delete(_id, item)
        return result

    def delete_many(self, **kwargs):
        items = self.get_all(**kwargs)
        if items:
            for item in items:
                self.delete(item['_id'])

    def index(self, _id, doc=None, wait=True):
        if not self.es_handler:
            return
        if not doc:
            doc = self.get(_id)
        self.before_index(_id, doc)
        self.es_handler.index(_id, doc, wait=wait)

    def index_all(self, _ids):
        current_ind = 0
        if not isinstance(_ids, list):
            _ids = list(_ids)
        while True:
            ids = _ids[current_ind:30]
            items, has_next, paginate_result = self.db_handler.paginate(fields=None, sort=None,
                                                                        **{'_id': {'$in': ids}})
            if not items:
                break
            for item in items:
                self.index(item['_id'], item, wait=False)
            if not has_next:
                break
            current_ind += 30
