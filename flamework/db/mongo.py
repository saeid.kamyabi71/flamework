from datetime import datetime

import math
from bson import ObjectId
from dateutil import tz

from flamework.db.helpers import get_mgo_agg_sort, get_mgo_agg_project


class BaseMgoDB:
    def __init__(self, db):
        self.db = db

    def _get_ids_by_query(self, q):
        ids = []
        current_page = 1
        per_page = 100
        while True:
            items, has_next, paginate_result = self.paginate({'_id'}, [('_id', -1)],
                                                             page=current_page,
                                                             per_page=per_page, **q)
            if not items:
                break
            for item in items:
                ids.append(item['_id'])
            if not has_next:
                break
            current_page += 1
        return ids

    def paginate(self, fields, sort, page=1, per_page=30, **filters):
        filters['deleted'] = {'$ne': True}
        self.improve_filter(filters)

        query = list()
        query.append({"$match": filters})
        if fields:
            query.append({"$project": get_mgo_agg_project(fields)})
        if sort:
            query.append({"$sort": get_mgo_agg_sort(sort)})
        skip = per_page * (page - 1)
        limit = per_page + 1
        query.append({'$group': {'_id': None, 'total': {'$sum': 1}, 'results': {'$push': '$$ROOT'}}})
        # query.append({"$skip": skip})
        # query.append({"$limit": limit})
        query.append({'$project': {'total': 1, 'results': {'$slice': ['$results', skip, limit]}}})
        results = list(self.db.aggregate(query))
        result = results[0] if results else {}
        items = result.get('results', [])
        has_next = False
        if len(items) >= per_page:
            has_next = True
            items = items[:per_page]
        total_count = result.get('total', 0)
        page_count = math.ceil(total_count / per_page)
        paginate_result = {
            'total_count': total_count,
            'page_count': page_count,
            'page': page,
            'per_page': per_page,
            'has_next': has_next
        }
        return items, has_next, paginate_result

    def get_all(self, fields, sort, **filters):
        filters['deleted'] = {'$ne': True}
        self.improve_filter(filters)

        query = list()
        query.append({"$match": filters})
        if fields:
            query.append({"$project": get_mgo_agg_project(fields)})
        if sort:
            query.append({"$sort": get_mgo_agg_sort(sort)})
        items = list(self.db.aggregate(query))
        return items

    @staticmethod
    def improve_filter(filters):
        if '_id' in filters:
            if isinstance(filters['_id'], dict):
                for key, value in filters['_id'].items():
                    if isinstance(value, str):
                        filters['_id'][key] = ObjectId(filters['_id'][key])
                    elif isinstance(value, list):
                        for i in range(len(value)):
                            if isinstance(value[i], str):
                                filters['_id'][key][i] = ObjectId(filters['_id'][key][i])

            elif not isinstance(filters['_id'], ObjectId):
                filters['_id'] = ObjectId(filters['_id'])

    def get(self, _id, fields=None, **filters):
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)
        filters['deleted'] = {'$ne': True}
        filters['_id'] = _id

        query = list()
        query.append({"$match": filters})
        proj = get_mgo_agg_project(fields)
        if proj:
            query.append({"$project": proj})
        items = list(self.db.aggregate(query))
        return items[0] if items else None

    def find_one(self, fields=None, **filters):
        self.improve_filter(filters)
        filters['deleted'] = {'$ne': True}
        query = list()
        query.append({"$match": filters})
        proj = get_mgo_agg_project(fields)
        if proj:
            query.append({"$project": proj})
        query.append({"$limit": 1})
        items = list(self.db.aggregate(query))
        return items[0] if items else None

    def exists(self, **filters):
        return self.find_one(**filters) is not None

    def insert(self, **properties):
        properties["create_date"] = datetime.now().astimezone(tz=tz.tzutc())
        properties["modify_date"] = datetime.now().astimezone(tz=tz.tzutc())
        _id = self.db.insert_one(properties).inserted_id
        properties['_id'] = _id
        return properties

    def update(self, _id, **properties):
        query = dict()
        keys = list(properties.keys())
        for key in keys:
            if key.startswith('$'):
                query[key] = properties[key]
                del properties[key]
        properties["modify_date"] = datetime.now().astimezone(tz=tz.tzutc())
        query["$set"] = properties
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)
        result = self.db.update({"_id": _id}, query)
        return result['nModified'] == 1

    def update_many(self, filters, update, ignore_deleted=True):
        if ignore_deleted:
            filters['deleted'] = {'$ne': True}
        result = self.db.update_many(filters, update)
        return result.modified_count >= 1

    def delete(self, _id):
        if not isinstance(_id, ObjectId):
            _id = ObjectId(_id)
        return self.update(_id, deleted=True)
