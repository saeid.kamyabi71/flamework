import json

import redis
from flamework.extensions import redis_pool


class BaseRedisDB:
    @classmethod
    def __client(cls):
        """Get new instance of redis client
        :return: Instance of redis client
        """
        return redis.Redis(connection_pool=redis_pool())

    @classmethod
    def get_from_redis(cls, key):
        """Get existing item with given key
        :param key: key to get value
        :return: Value if exists, Otherwise returns None
        """
        r = cls.__client()
        info = r.get(key)
        if info:
            return json.loads(info.decode())
        return None

    @classmethod
    def save_to_redis(cls, key, data, ex):
        """Save data with given key with
        expiration with given ex as seconds
        :param key: key to save data with.
        :param data: data to save in redis
        :param ex: expiration time in seconds
        """
        existing_info = cls.get_from_redis(key)
        if existing_info:
            existing_info.update({k: v for k, v in data.items() if v is not None})
            data = existing_info

        data_str = json.dumps(data)
        r = cls.__client()
        r.set(key, data_str, ex=ex)

    @classmethod
    def delete_redis_by_key(cls, key):
        """Delete existing item from db with given key
        :param key: key to remove data
        :return: True if deleted, Otherwise returns False
        """
        r = cls.__client()
        result = r.delete(key)
        return result is not None
