import math
import copy

from flamework.helpers.dict import make_serializable, purge
from flamework.extensions import es
from flamework.helpers.event import EventHandler


class BaseElastic(EventHandler):
    __index_name__ = None
    __doc_type__ = 'doc'
    __mapping__ = None
    __query_mapper__ = None

    def populate_search_hit(self, hit, fields, sort, filters):
        self.fire_event('populate_search_hit', hit, fields, sort, filters)
        return hit

    def populate_search_result(self, result, fields, sort, filters):
        items = list()
        ind = 0
        distance_ind = None
        for _, field, _ in sort:
            if field == '_geo_distance':
                distance_ind = ind
            ind += 1
        for hit in result['hits']['hits']:
            hit = self.populate_search_hit(hit, fields, sort, filters)
            source = hit['_source']
            source['_id'] = hit['_id']
            items.append(source)
            purge(source, fields)
            if distance_ind is not None:
                _sort = hit.get('sort')
                if _sort:
                    source['distance'] = int(_sort[distance_ind])
        return items

    def before_paginate_query_generate(self, fields, sort, page, per_page, filters):
        return fields, sort, page, per_page, filters

    def before_search(self, fields, sort, page, per_page, filters, query):
        return fields, sort, page, per_page, filters, query

    def paginate(self, fields, sort, page=1, per_page=30, **filters):
        fields, sort, page, per_page, filters = self.before_paginate_query_generate(fields, sort, page,
                                                                                    per_page, filters)
        query = filters
        if self.__query_mapper__:
            query = self.__query_mapper__.generate(query, sort)
        fields, sort, page, per_page, filters, query = self.before_search(fields, sort, page,
                                                                          per_page, filters, query)
        make_serializable(query)
        result = es.search(self.__index_name__, self.__doc_type__, body=query,
                           from_=(page - 1) * per_page, size=per_page+1)
        items = self.populate_search_result(result, fields, sort, filters)
        has_next = False
        if len(items) > per_page:
            items = items[:-1]
            has_next = True
        total_count = result['hits']['total']
        page_count = math.ceil(total_count / per_page)
        paginate_result = {
            'total_count': total_count,
            'page_count': page_count,
            'page': page,
            'per_page': per_page,
            'has_next': has_next
        }
        return items, has_next, paginate_result

    def before_index(self, doc):
        pass

    def index(self, _id, doc, wait=False):
        if not doc:
            return False
        doc = copy.deepcopy(doc)
        if self.__mapping__:
            doc = purge(doc, self.__mapping__.keys())
        self.before_index(doc)
        make_serializable(doc)
        params = dict()
        if wait:
            params['refresh'] = 'true'  #'wait_for'
        result = es.index(self.__index_name__, self.__doc_type__, doc, id=_id, **params)
        indexed = result['result'] in ['created', 'updated']
        return indexed

    def insert(self, **properties):
        if self.index(properties['_id'], properties):
            return properties
        return None

    def update(self, _id, wait=False, **properties):
        return self.index(_id, properties, wait=wait)

    def delete(self, _id, wait=False):
        params = dict()
        if wait:
            params['refresh'] = 'wait_for'
        result = es.delete(self.__index_name__, self.__doc_type__, _id, **params)
        return result['result'] in ['deleted']
