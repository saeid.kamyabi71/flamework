def get_mgo_agg_sort(sort):
    s = {}
    for _, field, order in sort:
        s[field] = order
    return s


def get_mgo_agg_project(fields):
    p = {}
    if fields:
        for field in fields:
            p[field] = 1
    return p
