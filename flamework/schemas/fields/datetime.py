from datetime import datetime

from dateutil import tz
from marshmallow import fields

from flamework.helpers.time import serialize_datetime


class CDateTime(fields.DateTime):
    def __init__(self, date_only=False, **kwargs):
        self.date_only = date_only
        super(CDateTime, self).__init__(**kwargs)

    def _deserialize(self, value, attr, data):
        if self.date_only:
            if 'T' not in value:
                value += 'T00:00:00'
        value = super(CDateTime, self)._deserialize(value, attr, data)
        if value:
            if not value.tzinfo:
                # value = value.astimezone(tz=tz.tzutc())
                value = value.replace(tzinfo=tz.tzutc())
            else:
                value = (value - value.utcoffset()).replace(tzinfo=tz.tzutc())
        if self.date_only:
            value = datetime(value.year, value.month, value.day)
        return value

    def _serialize(self, value, attr, obj):
        return serialize_datetime(value)
