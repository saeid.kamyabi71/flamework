from marshmallow import fields, ValidationError

from flamework.helpers.normalize.phone import normalize_phone


class Phone(fields.Field):
    def _deserialize(self, value, attr, data):
        phone = normalize_phone(value)
        if phone is None:
            raise ValidationError('Invalid value for phone number')
        return phone.title()
