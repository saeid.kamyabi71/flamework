import re
from marshmallow import fields, ValidationError


class Password(fields.Field):
    def _deserialize(self, value, attr, data):
        if not re.match(r'^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})', value):
            raise ValidationError('Password must contain at least 8 characters '
                                  'including digits, upper/lower case')
        return value
