from marshmallow import Schema, fields

from flamework.models.consts import UserRoles
from flamework.schemas.core.field_types.object_loader import ObjectLoaderFieldType
from flamework.schemas.fields.datetime import CDateTime


class SuperSchema(Schema):
    def __init__(self, *args, roles=None, **kwargs):
        super(SuperSchema, self).__init__(*args, **kwargs)

        self.roles = roles
        self.process_fields()

    def process_field(self, name, field):
        pass

    def process_fields(self):
        # if hasattr(cls, 'fields_initialized') and cls.fields_initialized:
        #     return
        for name, field in self.declared_fields.items():
            if not field.metadata:
                continue
            self.process_field(name, field)
        # cls.fields_initialized = True
        
    def dump_item(self, data_item):
        return data_item

    def dump(self, obj, many=None, update_fields=True, **kwargs):
        data_list = obj if many or self.many else [obj]
        for data_item in data_list:
            self.dump_item(data_item)
        return super(SuperSchema, self).dump(obj, many=many, update_fields=update_fields, **kwargs)

    def load_item(self, data_item):
        return data_item

    def load(self, data, many=None, partial=None):
        data_list = data if many or self.many else [data]
        for data_item in data_list:
            self.load_item(data_item)
        return super(SuperSchema, self).load(data, many, partial)
    

class FieldTypeHandlerSchema(SuperSchema):
    __field_types = dict()

    def __init__(self, *args, **kwargs):
        self.field_type_handlers = {
            ObjectLoaderFieldType.LABEL: ObjectLoaderFieldType
        }
        super(FieldTypeHandlerSchema, self).__init__(*args, **kwargs)

    def process_field(self, name, field):
        types = field.metadata.get('types')
        if types:
            cls_name = self.__class__.__name__
            if cls_name not in self.__field_types:
                self.__field_types[cls_name] = dict()
            for t in types:
                extra = []
                if isinstance(t, tuple):
                    extra = t[1:]
                    t = t[0]
                if t not in self.__field_types[cls_name]:
                    self.__field_types[cls_name][t] = dict()
                self.__field_types[cls_name][t][name] = (field, extra)
        return super(FieldTypeHandlerSchema, self).process_field(name, field)
    
    def dump_item(self, data_item):
        # handle field types
        _f_types = self.__field_types.get(self.__class__.__name__)
        if _f_types:
            for _type, fields_map in _f_types.items():
                if _type not in self.field_type_handlers:
                    continue
                for _name, (_field, _extra) in fields_map.items():
                    v = self.field_type_handlers[_type].dump(data_item, data_item.get(_name), extra=_extra)
                    if v is not None or _name in data_item:
                        data_item[_name] = v
        return super(FieldTypeHandlerSchema, self).dump_item(data_item)
    
    def load_item(self, data_item):
        # handle field types
        _f_types = self.__field_types.get(self.__class__.__name__)
        if _f_types:
            for _type, fields_map in _f_types.items():
                if _type not in self.field_type_handlers:
                    continue
                for _name, _field in fields_map.items():
                    if _name not in data_item:
                        continue
                    data_item[_name] = self.field_type_handlers[_type].load(data_item, data_item[_name])
        return super(FieldTypeHandlerSchema, self).load_item(data_item)


class EditableSchema(SuperSchema):
    __filtered_editable_fields = dict()
    # __non_editable_fields = dict()

    def __init__(self, *args, editing=False, **kwargs):
        self.editing = editing

        super(EditableSchema, self).__init__(*args, **kwargs)

    def process_fields(self):
        if self.editing:
            fields_to_delete = []
            for name, field in self.declared_fields.items():
                if not field.metadata:
                    continue
                if 'editable' in field.metadata and not field.metadata['editable']:
                    fields_to_delete.append(name)
            for n in fields_to_delete:
                del self.fields[n]
        return super(EditableSchema, self).process_fields()

    def process_field(self, name, field):
        if 'edit_roles' in field.metadata:
            self.__filtered_editable_fields[name] = field
        return super(EditableSchema, self).process_field(name, field)

    def load_item(self, data_item):
        if self.editing:
            # handle editable fields
            filtered_editable_fields = self.__filtered_editable_fields
            for name, field in filtered_editable_fields.items():
                if name not in data_item:
                    continue
                has_access = False
                if self.roles:
                    for role in self.roles:
                        role_value = UserRoles.RMAP[role]
                        if role_value in field.metadata.get('edit_roles', []):
                            has_access = True
                            break
                if not has_access:
                    del data_item[name]
        # # handle non-editable fields
        # non_editable_fields = self.__non_editable_fields
        # for name, field in non_editable_fields.items():
        #     if name not in data_item:
        #         continue
        #     has_access = False
        #     if not has_access:
        #         del data_item[name]
        return super(EditableSchema, self).load_item(data_item)


class BaseSchema(FieldTypeHandlerSchema, EditableSchema):
    _id = fields.String(sortable=True, default_sort=(10, -1))
    create_date = CDateTime(sortable=True, edit_roles=[UserRoles.ADMIN])
    modify_date = CDateTime(sortable=True, edit_roles=[UserRoles.ADMIN])

    __fields = None
    __sort_fields = None
    __default_fields = None
    __default_sort = None
    __sort = None
    __filtered_editable_fields = None

    @classmethod
    def get_fields(cls):
        if cls.__fields is None:
            cls.__fields = set(cls._declared_fields)
        return cls.__fields

    @classmethod
    def get_sort_fields(cls):
        if cls.__sort_fields is None:
            cls.__sort_fields = []
            for name, field in cls._declared_fields.items():
                if field.metadata and field.metadata.get('sortable'):
                    cls.__sort_fields.append(name)
        return cls.__sort_fields

    @classmethod
    def get_default_fields(cls):
        if cls.__default_fields is None:
            cls.__default_fields = set()
            for name, field in cls._declared_fields.items():
                if name not in cls.get_fields():
                    continue
                if not field.metadata or field.metadata.get('default_field', True):
                    cls.__default_fields.add(name)
        return cls.__default_fields

    @classmethod
    def get_default_sort(cls):
        if cls.__default_sort is None:
            cls.__default_sort = []
            for name, field in cls._declared_fields.items():
                default_sort = field.metadata.get('default_sort') if field.metadata else None
                if default_sort:
                    order, sorder = default_sort
                    cls.__default_sort.append((order, name, sorder))
            if cls.__default_sort:
                cls.__default_sort = sorted(cls.__default_sort, key=lambda x: x[0])
        return cls.__default_sort

    @classmethod
    def get_sort(cls):
        if cls.__sort is None:
            cls.__sort = []
            for name, field in cls._declared_fields.items():
                default_sort = field.metadata.get('sort') if field.metadata else None
                if default_sort:
                    order, sorder = default_sort
                    cls.__sort.append((order, name, sorder))
            if cls.__sort:
                cls.__sort = sorted(cls.__sort, key=lambda x: x[0])
        return cls.__sort
