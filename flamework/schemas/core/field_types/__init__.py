class BaseFieldTypeConverter:
    LABEL = None

    @classmethod
    def dump(cls, data, data_item, extra=None):
        return data_item

    @classmethod
    def load(cls, data, data_item):
        return data_item


from .currency import CurrencyFieldType
from .price import PriceFieldType
from .object_loader import ObjectLoaderFieldType
from .remaining_handler import RemainingTimeHandlerFieldType
