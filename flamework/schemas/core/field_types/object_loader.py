from flamework.schemas.core.field_types import BaseFieldTypeConverter


class ObjectLoaderFieldType(BaseFieldTypeConverter):
    LABEL = 'object'

    _loaders = dict()

    @classmethod
    def register_loader(cls, key, func):
        cls._loaders[key] = func

    @classmethod
    def dump(cls, data, data_item, extra=None):
        otype, target_field = extra
        _id = data.get(target_field)
        if not _id:
            return None
        return cls._loaders[otype](_id)

    @classmethod
    def load(cls, data, data_item, **kwargs):
        return super(ObjectLoaderFieldType, cls).load(data, data_item)
