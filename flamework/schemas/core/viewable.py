from marshmallow import fields


class Viewable(object):
    view_count = fields.Integer(sortable=True)
