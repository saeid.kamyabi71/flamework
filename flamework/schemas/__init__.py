"""
in marshmallow schema, custom fields are:

sortable=true/false => determines if field is sortable or not, default is false

default_sort=(order, desc/asc) => example: _id = fields.String(sortable=True, default_sort=(0, -1))
    this means that _id is sortable and default sort is descending by order of 0(higher order)


"""