from flamework.extensions import current_config


def get_elastic_settings(mapping, settings=None):
    # index settings
    elastic_settings = {
        "settings": settings or {
            "number_of_shards": current_config.ELASTIC['shards'],
            "number_of_replicas": current_config.ELASTIC['replicas'],
            "routing_partition_size": current_config.ELASTIC['routing_partition_size'],

            "analysis": {
                "analyzer": {
                    "ngram_analyzer": {
                        "type": "custom",
                        # "normalizer": "deep_normalizer", # not working, using char_filter instead!
                        "char_filter": ["number_char_filter_map",
                                        "deep_persian_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "ngram_tokenizer_whitespace",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "ngram_search_analyzer": {
                        "type": "custom",
                        # "normalizer": "deep_normalizer", # not working, using char_filter instead!
                        "char_filter": ["number_char_filter_map",
                                        "deep_persian_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "ngram_tokenizer_whitespace",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "deep_ngram_analyzer": {
                        "type": "custom",
                        "char_filter": ["remove_white_spaces",
                                        "number_char_filter_map",
                                        "deep_persian_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "ngram_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "deep_ngram_search_analyzer": {
                        "type": "custom",
                        "char_filter": ["remove_white_spaces",
                                        "number_char_filter_map",
                                        "deep_persian_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "ngram_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "word_analyzer": {
                        "type": "custom",
                        "char_filter": ["number_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "word_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "word_search_analyzer": {
                        "type": "custom",
                        "char_filter": ["number_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "word_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "keyword_analyzer": {
                        "type": "custom",
                        "char_filter": ["number_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "word_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    },
                    "keyword_search_analyzer": {
                        "type": "custom",
                        "char_filter": ["number_char_filter_map",
                                        "remove_duplicates"],
                        "tokenizer": "word_tokenizer",
                        "filter": [
                            "lowercase",
                            "english_stop",
                            "persian_stop",
                            "arabic_stop"
                        ]
                    }
                },
                "char_filter": {
                    "number_char_filter_map": {
                        "type": "mapping",
                        "mappings": [
                            "٠ => 0",
                            "١ => 1",
                            "٢ => 2",
                            "٣ => 3",
                            "٤ => 4",
                            "٥ => 5",
                            "٦ => 6",
                            "٧ => 7",
                            "٨ => 8",
                            "٩ => 9",

                            "۰ => 0",
                            "۱ => 1",
                            "۲ => 2",
                            "۳ => 3",
                            "۴ => 4",
                            "۵ => 5",
                            "۶ => 6",
                            "۷ => 7",
                            "۸ => 8",
                            "۹ => 9"
                        ]
                    },
                    "deep_persian_char_filter_map": {
                        "type": "mapping",
                        "mappings": [
                            "ئ => ا",
                            "أ => ا",
                            "ؤ => ا",
                            "ء => ا",
                            "ﻋ => ا",
                            "ﻉ => ا",
                            "إ => ا",
                            "آ => ا",

                            "ص => س",
                            "ث => س",

                            "ح => ه",

                            "ذ => ض",
                            "ز => ض",
                            "ظ => ض",

                            "ط => ت",
                            "ة => ت",

                            "غ => ق",

                            "ك => ک",
                            "ڪ => ک",

                            "ي => ی",
                        ]
                    },
                    "remove_duplicates": {
                        "type": "pattern_replace",
                        "pattern": "(.)(?=\\1)",
                        "replacement": ""
                    },
                    "remove_white_spaces": {
                        "type": "pattern_replace",
                        "pattern": "(\s)",
                        "replacement": ""
                    }
                },
                # "normalizer": { # not working using char_filter in analyzer
                #     "deep_normalizer": {
                #         "type": "custom",
                #         "char_filter": ["remove_white_spaces",
                #                         "number_char_filter_map",
                #                         "deep_persian_char_filter_map",
                #                         "remove_duplicates"],
                #         "filter": ["lowercase", "asciifolding"]
                #     }
                # },
                "filter": {
                    "english_stop": {
                        "type": "stop",
                        "ignore_case": True,
                        "stopwords": "_english_"
                    },
                    "persian_stop": {
                        "type": "stop",
                        "stopwords": "_persian_"
                    },
                    "arabic_stop": {
                        "type": "stop",
                        "stopwords": "_arabic_"
                    }
                },
                "tokenizer": {
                    # "edge_ngram_tokenizer": {
                    #     "type": "edge_ngram",
                    #     "min_gram": 2,
                    #     "max_gram": 64,
                    #     # "token_chars": [
                    #     #     "letter",
                    #     #     "digit",
                    #     #     "whitespace",
                    #     #     # "punctuation",
                    #     #     # "symbol",
                    #     # ]
                    # },
                    "ngram_tokenizer": {
                        "type": "ngram",
                        "min_gram": 2,
                        "max_gram": 7,
                        # "token_chars": [
                        #     "letter",
                        #     "digit",
                        #     "whitespace",
                        #     # "punctuation",
                        #     # "symbol",
                        # ]
                    },
                    "ngram_tokenizer_whitespace": {
                        "type": "ngram",
                        "min_gram": 2,
                        "max_gram": 7,
                        "token_chars": [
                            "letter",
                            "digit",
                            # "whitespace",
                            "punctuation",
                            "symbol",
                        ]
                    },
                    "word_tokenizer": {
                        "type": "standard",
                        "token_chars": [
                            # "letter",
                            # "digit",
                            # "whitespace",
                            # "punctuation",
                            # "symbol",
                        ]
                    }
                }
            }
        },
        "mappings": {
            "doc": mapping
        }
    }
    return elastic_settings
