import json
import pprint

import time

import redis
from elasticsearch import Elasticsearch
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_pymongo import PyMongo
from marshmallow.exceptions import ValidationError as MarshmallowValidationError

from .config import DefaultConfig
from .extensions import g, current_config, mgo, es, redis_pool
from .helpers.exceptions import ValidationError, NotFoundError, InternalServiceError, \
    ForbiddenError, InvalidOperationError, UnAuthorizedError
from .helpers.template.filters import register_filters
from .schemas.elasticsearch import get_elastic_settings, elastic_mappings

default_app_name = "flamework"


def create_app(config=None, app_name=default_app_name):
    if not config:
        config = DefaultConfig
    current_config.set(config)
    app = Flask(app_name,
                template_folder=current_config.TEMPLATE_PATH,
                static_folder=current_config.STATIC_PATH)

    CORS(app, resources={r"/*": {"origins": "*",
                                 "expose_headers": ["X-ACCESS-TOKEN",
                                                    "X-EXPIRES-IN",
                                                    "X-TOKEN-TYPE"]
                                 }
                         },
         supports_credentials=True)

    configure_app(app, config)
    configure_blueprints(app)
    configure_extensions(app)
    configure_custom_errors(app)
    configure_before_request(app)
    return app


def configure_app(app, config):
    app.config.from_object(config or DefaultConfig)


def configure_blueprints(app):
    api_versions = app.config["API_VERSIONS"]
    for api_version in api_versions:
        blueprints = __import__("project.controllers.{}".format(api_version), fromlist=[api_version])
        app.register_blueprint(blueprints.api_bp)


def configure_extensions(app):
    register_filters(app)

    redis_pool.set(redis.ConnectionPool(host=current_config.REDIS_HOST,
                                        port=current_config.REDIS_PORT))

    # mongo
    with app.app_context():
        mongo = PyMongo(app)
        mgo.set(mongo.db)

    # elastic
    retry_time = 3
    for i in range(10):
        try:
            elastic = Elasticsearch(hosts=app.config['ELASTIC']['hosts'],
                                    http_auth=(app.config['ELASTIC']['user'],
                                               app.config['ELASTIC']['password']))
            es.set(elastic)
            for key in elastic_mappings.keys():
                if not es.indices.exists(key):
                    elastic.indices.create(index=key, body=get_elastic_settings(key))
            break
        except Exception as ex:
            print("initial of elastic faced with error")
            print(ex)
            if i < 10:
                print("retrying %s..." % (i + 1))
                time.sleep(retry_time * i)


def configure_custom_errors(app):
    @app.errorhandler(InternalServiceError)
    def internal_error(error):
        _error = error.service_error if error.status_code != 500 else 'Unfortunately service faced with error'
        if isinstance(_error, bytes):
            _error = _error.decode('utf-8')
        try:
            _error = json.loads(_error) or dict(message=_error)
        except:
            _error = dict(message=_error)
        if error.error:
            err_msg = _error.get('message', '')
            if err_msg:
                _error['message'] = error.error + '\r\n' + _error['message']
            else:
                _error['message'] = error.error
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), error.status_code

    @app.errorhandler(ValidationError)
    def validation_error(error):
        _error = dict(message='Invalid request information')
        if error.field and error.error:
            _error['errors'] = {
                error.field: [error.error]
            }
        elif error.error:
            _error['message'] = error.error
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), 400

    @app.errorhandler(NotFoundError)
    def validation_error(error):
        _error = dict(message='Requested information not found')
        if error.field and error.error:
            _error['errors'] = {
                error.field: [error.error]
            }
        elif error.error:
            _error['message'] = error.error
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), 404

    @app.errorhandler(MarshmallowValidationError)
    def marshmallow_validation_error(error):
        _error = dict(message='Invalid request information')
        _error['errors'] = error.messages
        return jsonify(_error), 400

    @app.errorhandler(UnAuthorizedError)
    def forbidden_error(error):
        _error = dict(message="UnAuthorized")
        if error.field and error.error:
            _error['errors'] = {
                error.field: [error.error]
            }
        elif error.error:
            _error['message'] = error.error
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), 401

    @app.errorhandler(ForbiddenError)
    def forbidden_error(error):
        _error = dict(message="You don't have access to this section")
        if error.field and error.error:
            _error['errors'] = {
                error.field: [error.error]
            }
        elif error.error:
            _error['message'] = error.error
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), 403

    @app.errorhandler(InvalidOperationError)
    def invalid_operation_error(error):
        _error = dict(message=str(error))
        if error.status is not None:
            _error['status'] = error.status
        return jsonify(_error), 409


def configure_before_request(app):
    @app.before_request
    def before_request():
        g.set(dict(state=0))

        do_log = True
        if request.method in ('GET', 'OPTIONS'):
            if '/hotel_orders' in request.path or '/cip_orders' or '/orders' in request.path:
                do_log = False
        if do_log:
            print(pprint.pformat(request.environ, depth=5))
            print("BODY:")
            data = request.get_data()
            try:
                print(data.decode("utf-8"))
            except Exception as ex:
                print('Exception on log req body: %s' % ex)
