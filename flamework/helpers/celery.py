from functools import wraps

from flamework.extensions import celery, current_config


def celery_task(max_retries=3, countdown=5):
    def _celery_task(f):
        @wraps(f)
        def wrapper(_f, *args, **kwargs):
            try:
                f(*args, **kwargs)
            except Exception as ex:
                print("exception on celery_task `%s`" % _f.__name__)
                print(ex)
                raise _f.retry(countdown=(_f.request.retries+1) * countdown, exc=ex)

        r = celery.task(wrapper, bind=True, max_retries=max_retries)
        return r
    return _celery_task


def run_task(f, *args, **kwargs):
    if current_config.DEVELOPMENT:
        try:
            f(*args, **kwargs)
        except Exception as ex:
            print("exception on run_task `%s`" % f.__name__)
            print(ex)
    else:
        f.delay(*args, **kwargs)
