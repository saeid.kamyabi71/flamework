from project.extensions import g
from project.models.consts import UserRoles


def account_has_role(*roles):
    """Check if current account has role
    :param roles: roles to check
    :return: True if has role, Otherwise returns False
    """
    if not hasattr(g, 'roles') or not g.roles:
        return False
    for role in roles:
        if UserRoles.MAP[role] in g.roles:
            return True
    return False


def has_user_access(user_id):
    if not hasattr(g, 'user_id'):
        return False
    if account_has_role(UserRoles.ADMIN) or g.user_id == user_id:
        return True
    return False
