import copy


class ElasticQueryGenerator:
    query_templates = None
    query_fill_paths = None
    
    def __init__(self, query_templates, query_fill_paths):
        self.query_templates = query_templates
        self.query_fill_paths = query_fill_paths
    
    @staticmethod
    def set_nested(data, value, *args):
        if args and data:
            if args[0] == '$':
                for item in data:
                    ElasticQueryGenerator.set_nested(item, value, *args[1:])
                return
            element = args[0]
            if element:
                if len(args) == 1:
                    data[element] = value
                    return
                x = data.get(element)
                ElasticQueryGenerator.set_nested(x, value, *args[1:])

    def generate(self, filters, sort=None):
        must_must = []
        result = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'bool': {
                                'must': must_must
                            }
                        },
                        # {
                        #     'bool': {
                        #         'should': must_should
                        #     }
                        # }
                    ],
                    # 'should': [],
                    # 'must_not': [],
                    # 'filter': {
                    #     'bool': {
                    #         'must': []
                    #     }
                    # }
                }
            }
        }
        if sort:
            s = []
            for _, field, order in sort:
                if isinstance(order, int):
                    s.append({
                        field: {'order': ('desc' if order == -1 else 'asc')}
                    })
                else:
                    s.append({field: order})
            if s:
                s.append('_score')
                result['sort'] = s
        else:
            result['sort'] = [
                '_score',
                {'_id': 'desc'}
            ]

        def add_template(field_name, array, value):
            if field_name in self.query_templates:
                if not self.query_templates[field_name]:
                    return
                template = copy.deepcopy(self.query_templates[field_name])
                query_fill_paths = self.query_fill_paths[field_name]
                for path_parts in query_fill_paths:
                    ElasticQueryGenerator.set_nested(template, value, *path_parts)
                array.append(template)
            else:
                array.append({
                    "match": {
                        field_name: value
                    }
                })

        def generate_operator(op):
            if op in ['$and', '$all']:
                a = []
                return {
                    'bool': {
                        'must': a
                    }
                }, a
            elif op in ['$or', '$in']:
                a = []
                return {
                    'bool': {
                        'should': a
                    }
                }, a
            return None, None

        def generate_cond(existing, op, key, value):
            if not existing:
                existing.update({
                    'range': {
                        key: {
                        }
                    }
                })
            if op == '$lt':
                existing['range'][key]['lt'] = value
            elif op == '$lte':
                existing['range'][key]['lte'] = value
            elif op == '$gt':
                existing['range'][key]['gt'] = value
            elif op == '$gte':
                existing['range'][key]['gte'] = value

        def _iterate(data, root, temp=None):
            op_map = {}
            if isinstance(data, dict):
                for key, value in data.items():
                    if key in {'$and', '$or', '$all', '$in'}:  # operators
                        q, a = generate_operator(key)
                        for val in value:
                            _iterate(val, a, temp)
                        root.append(q)
                    elif key in {'$lt', '$lte', '$gt', '$gte'}:
                        exists = True
                        if temp not in op_map:
                            op_map[temp] = {}
                            exists = False
                        generate_cond(op_map[temp], key, temp, value)
                        if not exists:
                            root.append(op_map[temp])
                    else:
                        if isinstance(value, dict):
                            _iterate(value, root, temp=key)
                        else:
                            add_template(key, root, value)
            elif isinstance(data, str):
                if temp:
                    add_template(temp, root, data)
        _iterate(filters, must_must)
        return result
