import re


def normalize_phone(phone):
    if not phone:
        return None
    r = re.findall("^(?:0|\+)(\d{5,15})$", phone)
    if not r:
        return None
    return phone
