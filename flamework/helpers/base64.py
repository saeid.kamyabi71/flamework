import base64


def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += bytearray('=' * (4 - missing_padding), 'UTF-8')
    return base64.decodebytes(data)
