
class EventHandler:
    _delegates = {}

    def register_event(self, func, callback):
        fname = func.__name__
        if fname not in self._delegates:
            self._delegates[fname] = set()
        self._delegates[fname].add(callback)

    def fire_event(self, fname, *args, **kwargs):
        if fname in self._delegates:
            for func in self._delegates[fname]:
                func(*args, **kwargs)
