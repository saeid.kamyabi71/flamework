from base64 import b64encode


class CException(Exception):
    def __init__(self, *args, status=None, **kwargs):
        super().__init__(args, kwargs)
        self.field = args[0] if args and len(args) >= 2 else None or kwargs.get('field')
        self.error = args[1] if self.field else args[0] if args else None or kwargs.get('error')
        self.status = status


class NotFoundError(CException):
    """not found exception"""
    pass


class InvalidOperationError(CException):
    """invalid operation exception for
    errors on external requests"""
    pass


class ForbiddenError(CException):
    """Forbidden exception"""
    pass


class UnAuthorizedError(CException):
    """UnAuthorized exception"""
    pass


class ValidationError(CException):
    """validation exception"""
    pass


class InternalServiceError(CException):
    """internal service exception"""
    def __init__(self, status_code, service_error, error=None):
        self.status_code = status_code
        self.service_error = service_error
        self.error = error


def log_exception(ex):
    """log exception
    :param ex: exception to log
    :type ex: Exception
    """
    ex_str = ex.__repr__()
    if isinstance(ex_str, bytes):
        ex_str = ex_str.decode('utf-8')
    try:
        print(ex_str)
    except:
        print('can not log exception, base64 encoded error:')
        print(b64encode(ex_str.encode()))


class ExceptionStatusCodes:
    BEING_USED = 4001
    ALREADY_EXISTS = 4002
