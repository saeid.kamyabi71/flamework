import os

from flask import url_for

from flamework.extensions import current_config


def static_path(filename):
    if current_config.TESTING:
        return url_for('static', filename=filename, _external=True)
    return os.path.join(current_config.STATIC_PATH, filename)


def register_filters(app):
    app.jinja_env.globals['static_path'] = static_path
