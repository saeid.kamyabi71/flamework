class LocalProxy(object):
    __value__ = None

    def __getattr__(self, item):
        if hasattr(self.val, item):
            return getattr(self.val, item)
        assert AttributeError('No such attribute %s exists in current object' % item)

    def __getitem__(self, item):
        return self.val[item]

    def __call__(self):
        return self.val

    @property
    def initialized(self):\
        return self.__value__ is not None

    def set_value(self, name, value):
        """set specific value to current object
        :param name: name of attribute to set
        :type name: str
        :param value: value of attribute
        """
        setattr(self, name, value)

    def set(self, item):
        """set the item as the current value.
        :param item: instance to assign for current object
        """
        self.__value__ = item

    def unset(self):
        """Removes the current item"""
        self.__value__ = None

    @property
    def val(self):
        """
        :returns: The assigned value of current item.
        """
        if not self.__value__:
            raise LookupError('Local Proxy is not initialized.'
                              'Trying to use proxy instance before its assignment?!')
        return self.__value__