"""dictionary helpers
"""

from datetime import datetime, timezone

from bson import ObjectId


def purge(dictionary, keys):
    """purge dictionary and keep only given keys
    :param dictionary: dictionary to purge
    :type dictionary: dict
    :param keys: keys to keep in given dictionary
    :type keys: list
    :return: purged dictionary
    """
    keys_to_remove = [k for k in dictionary.keys() if k not in keys]
    for k in keys_to_remove:
        del dictionary[k]
    return dictionary


def make_serializable(doc):
    def fix_instance(value):
        if isinstance(value, ObjectId):
            return str(value)
        if isinstance(value, datetime):
            if not value.tzinfo:
                value = value.replace(tzinfo=timezone.utc)
            return datetime.strftime(value, '%Y-%m-%d %H:%M:%S.%f%z')
        if isinstance(value, dict):
            return make_serializable(value)
        if isinstance(value, list):
            return make_serializable(value)
        return value

    if isinstance(doc, dict):
        for key, _value in doc.items():
            doc[key] = fix_instance(_value)
    elif isinstance(doc, list):
        for i in range(len(doc)):
            doc[i] = fix_instance(doc[i])
    return doc
