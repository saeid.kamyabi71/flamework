"""
"""

from datetime import datetime
import dateutil.parser
from dateutil import tz


def check_ts(value, ts_min=-62135609144.0, ts_max=253402288200.0):
    """check is timestamp is correct or not
    :param value: value to check
    :type ts_min: callable or float
    :type ts_max: callable or float
    :return: True if is timestamp, otherwise returns False
    :rtype: bool
    """
    ts_min = ts_min().timestamp() if hasattr(ts_min, '__call__') else ts_min
    ts_max = ts_max().timestamp() if hasattr(ts_max, '__call__') else ts_max
    try:
        datetime.fromtimestamp(value)
        if ts_min <= value < ts_max:
            return True
    except:
        pass
    return False


def serialize_datetime(value):
    if isinstance(value, str):
        # value = ''.join(value.rsplit(':', 1))
        value = datetime.strptime(value, '%Y-%m-%d %H:%M:%S.%f%z')
    value = value.replace(tzinfo=None)
    return value.isoformat()


def deserialize_datetime(value, only_date=False):
    if not value:
        return None
    if isinstance(value, str):
        value = dateutil.parser.parse(value)
    if not value.tzinfo:
        # value = value.astimezone(tz=tz.tzutc())
        value = value.replace(tzinfo=tz.tzutc())
    else:
        value = (value - value.utcoffset()).replace(tzinfo=tz.tzutc())
    if only_date:
        value = datetime(value.year, value.month, value.day)
        return deserialize_datetime(value)
    return value
