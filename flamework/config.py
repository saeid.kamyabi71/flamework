"""configurations of project
"""
import os


class DefaultConfig(object):
    TESTING = True
    DEBUG = True
    DEVELOPMENT = False

    REDIS_HOST = os.environ.get('REDIS_HOST', '127.0.0.1')
    REDIS_PORT = os.environ.get('REDIS_PORT', 6379)

    MONGO_HOST = os.getenv("MONGO_HOST", "127.0.0.1")
    MONGO_PORT = os.getenv("MONGO_PORT", "27017")
    MONGO_DBNAME = os.getenv("MONGO_DBNAME", "flamework")
    MONGO_USERNAME = os.getenv("MONGO_USERNAME")
    MONGO_PASSWORD = os.getenv("MONGO_PASSWORD")
    MONGO_AUTH_SOURCE = os.getenv("MONGO_AUTH_SOURCE")

    ELASTIC = {
        'hosts': os.environ.get('ELASTIC_HOSTS', 'localhost:9200').split(','),
        'user': os.environ.get('ELASTIC_USER', ''),
        'password': os.environ.get('ELASTIC_PASSWORD', ''),
        'shards': int(os.environ.get('ELASTIC_SHARDS', 1)),
        'replicas': int(os.environ.get('ELASTIC_REPLICAS', 1)),
        'routing_partition_size': int(os.environ.get('ELASTIC_ROUTING_PARTITION_SIZE', 1))
    }

    CLIENT_SECRET = os.getenv("CLIENT_SECRET", "JkZxd0w2QkVUOFFUcWpDVyU3Xl8zUndlRiE9aHg0OSE=")

    PROJECT_PATH = os.getenv("PROJECT_PATH", os.path.dirname(__file__))
    UPLOAD_PATH = os.getenv("UPLOAD_PATH", os.path.join(PROJECT_PATH, "media/"))
    img_max_size_split = os.getenv("IMAGE_MAX_SIZE", "1536,1536").split(',')
    IMAGE_MAX_SIZE = (int(img_max_size_split[0]), int(img_max_size_split[1]))

    TEMPLATE_PATH = os.path.join(PROJECT_PATH, 'templates')
    STATIC_PATH = os.path.join(PROJECT_PATH, 'static')

    # celery configs
    BROKER_URL = os.getenv("CELERY_BROKER", "redis://localhost")


class TestConfig(DefaultConfig):
    TESTING = True
