"""extensions of project will be placed in this file.
for extensions which needs to be initialized on assignment use
LocalProxy

Example usage:
for example to create elastic search extension create simple *LocalProxy* 
object and assign to graph:
``
es = LocalProxy()
``

to initialize graph object call the **set** method of object:
``
es.set(ElasticSearch(...))
``

.. note::
   According to the project requirements you may not use
    *LocalProxy* if you don't need to. 
"""
from celery import Celery

from .helpers.local_proxy import LocalProxy


current_config = LocalProxy()
es = LocalProxy()
redis_pool = LocalProxy()

mgo = LocalProxy()

celery = Celery()

g = LocalProxy()
