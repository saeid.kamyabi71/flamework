class Viewable(object):
    @classmethod
    def logic_get(cls, *args, **kwargs):
        return super(Viewable, cls).logic_get(*args, **kwargs, increase_view_count=True)
