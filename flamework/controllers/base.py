import json

from datetime import datetime
from json import JSONDecodeError

from flask import request, g
from flask_restful import Resource

from .helpers.fetch import get_fetch_fields, get_sort_fields
from .helpers.paginate import pagination
from flamework.helpers.exceptions import ValidationError
from flamework.schemas.core.base import BaseSchema


class BaseController(Resource):
    __schema__ = None
    __logic_handler__ = None

    @classmethod
    def parse_filter(cls, key, value):
        return key, value, False

    @classmethod
    def load_with_datetime(cls, pairs, formats=list(['%Y-%m-%dT%H:%M:%S.%fZ', '%Y-%m-%dT%H:%M:%S', '%Y-%m-%d'])):
        """Load with dates"""
        d = {}
        for k, v in pairs:
            key, value, skip = cls.parse_filter(k, v)
            if skip:
                if key:
                    d[key] = value
                continue
            if isinstance(v, str):
                for _format in formats:
                    try:
                        d[k] = datetime.strptime(v, _format)
                        break
                    except ValueError:
                        d[k] = v
            else:
                d[k] = v
        return d

    @classmethod
    def get_filter(cls):
        filters = request.args.get('filters')
        if filters:
            try:
                filters = json.loads(filters, object_pairs_hook=cls.load_with_datetime)
            except JSONDecodeError:
                raise ValidationError('Invalid filter')
        else:
            filters = dict()
        return filters
    
    @classmethod
    def schema(cls, *args, **extra):
        if issubclass(cls.__schema__, BaseSchema):
            if hasattr(g, 'roles'):
                extra['roles'] = g.roles
        return cls.__schema__(*args, **extra)


class BulkBaseItemsController(BaseController):
    @classmethod
    def _get_all(cls, sort=None, **extra_filters):
        allowed_fields = get_fetch_fields(cls.__schema__.get_fields())
        sort_fields = sort or get_sort_fields(cls.__schema__.get_sort_fields())

        result = cls.__logic_handler__.get_all(fields=allowed_fields, sort=sort_fields, **extra_filters)
        result = cls.schema(many=True).dump(result)[0]
        return result

    @classmethod
    def logic_bulk_upsert(cls, data, **extra_data):
        result = cls.__logic_handler__.bulk_upsert(data, **extra_data)
        return result

    @classmethod
    def _post(cls, **extra_data):
        data = cls.schema(many=True, strict=True).load(request.get_json() or {})[0] or {}
        result = cls.logic_bulk_upsert(data, **extra_data)
        if not result:
            return None, 204
        return cls.schema().dump(result)[0], 201


class BaseItemsController(BaseController):
    @classmethod
    @pagination()
    def _search(cls, **extra_filters):
        allowed_fields = get_fetch_fields(cls.__schema__.get_fields())
        sort_fields = get_sort_fields(cls.__schema__.get_sort_fields())
        filters = cls.get_filter()
        filters.update(extra_filters)

        result, has_next, paginate_result = cls.__logic_handler__.search(page=g.page,
                                                                         per_page=g.per_page,
                                                                         fields=allowed_fields,
                                                                         sort=sort_fields,
                                                                         **filters)
        result = cls.schema(many=True).dump(result)[0]
        return {'result': result, 'has_next': has_next, 'pagination': paginate_result}

    @classmethod
    @pagination()
    def _get(cls, page=None, per_page=None, sort=None, **extra_filters):
        allowed_fields = get_fetch_fields(cls.__schema__.get_fields())
        sort_fields = sort or get_sort_fields(cls.__schema__.get_sort_fields())
        filters = cls.get_filter()
        filters.update(extra_filters)

        result, has_next, paginate_result = cls.__logic_handler__.paginate(page=page or g.page,
                                                                           per_page=per_page or g.per_page,
                                                                           fields=allowed_fields, sort=sort_fields,
                                                                           **filters)
        result = cls.schema(many=True).dump(result)[0]
        return {'result': result, 'has_next': has_next, 'pagination': paginate_result}

    @classmethod
    def _get_all(cls, sort=None, **extra_filters):
        allowed_fields = get_fetch_fields(cls.__schema__.get_fields())
        sort_fields = sort or get_sort_fields(cls.__schema__.get_sort_fields())
        filters = cls.get_filter()
        filters.update(extra_filters)

        result = cls.__logic_handler__.get_all(fields=allowed_fields, sort=sort_fields, **filters)
        result = cls.schema(many=True).dump(result)[0]
        return result

    @classmethod
    def logic_post(cls, **data):
        result = cls.__logic_handler__.new(**data)
        return result

    @classmethod
    def _post(cls, **extra_data):
        data = cls.schema(strict=True).load(request.get_json() or {})[0] or {}
        data.update(extra_data)
        result = cls.logic_post(**data)
        if not result:
            return None, 204
        return cls.schema().dump(result)[0], 201


class BaseItemController(BaseController):
    @classmethod
    def logic_get(cls, *args, **kwargs):
        return cls.__logic_handler__.get(*args, **kwargs)

    @classmethod
    def _get(cls, _id, **extra_filters):
        if not _id:
            filters = cls.get_filter()
            filters.update(extra_filters)
        else:
            filters = extra_filters
        allowed_fields = get_fetch_fields(cls.__schema__.get_fields())
        result = cls.logic_get(_id, fields=allowed_fields, **filters)
        return cls.schema().dump(result)[0]

    @classmethod
    def logic_update(cls, _id, **data):
        cls.__logic_handler__.update(_id, **data)

    @classmethod
    def _put(cls, _id, **extra_data):
        data = cls.schema(strict=True, editing=True).load(request.get_json() or {})[0] or {}
        data.update(extra_data)
        if '_id' in data:
            del data['_id']
        cls.logic_update(_id, **data)
        return '', 204

    @classmethod
    def logic_delete(cls, _id, **kwargs):
        cls.__logic_handler__.delete(_id, **kwargs)

    @classmethod
    def _delete(cls, _id, **kwargs):
        cls.logic_delete(_id, **kwargs)
        return '', 204
