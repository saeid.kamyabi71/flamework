from functools import wraps

from flask import request

from project.logic.currencies import SystemCurrency


def ignore_currency(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.method == 'OPTIONS':
            return f(*args, **kwargs)
        with SystemCurrency():
            return f(*args, **kwargs)
    return wrapper
