import jwt
from base64 import urlsafe_b64decode

import requests
from flask import request, abort, g
from functools import wraps

from project.extensions import current_config, g as current_g
from project.helpers.exceptions import ForbiddenError
from project.models.consts import UserRoles


def base_auth():
    if 'Authorization' in request.headers:
        token = request.headers['Authorization'][7:]
    else:
        token = request.cookies.get('access_token') or request.cookies.get('accessToken')
    secret = current_config.CLIENT_SECRET
    jwt_result = jwt.decode(token,
                            urlsafe_b64decode(secret),
                            algorithms=['HS256'], audience=current_config.CLIENT_ID,
                            options={'verify_nbf': False, 'verify_iat': False})
    g.sso_id = jwt_result.get('nameid')
    g.user_id = current_g.user_id = jwt_result['id']
    g.roles = current_g.roles = jwt_result.get("role", [])


def optional_login(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.method == 'OPTIONS':
            return f(*args, **kwargs)
        try:
            base_auth()
            g.logged_in = True
        except Exception as ex:  # any kind of error(signature exp, invalid header, invalid payload and etc.)
            g.logged_in = False
        return f(*args, **kwargs)
    return wrapper


def login_required(**options):
    """
    :param roles: acceptable roles
    :type roles: list of str
    """
    acceptable_roles = options.get('roles')

    def _login_required(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if request.method == 'OPTIONS':
                return f(*args, **kwargs)
            try:
                base_auth()

                if acceptable_roles:
                    has_access = False
                    for acceptable_role in acceptable_roles:
                        if UserRoles.MAP[acceptable_role] in g.roles:
                            has_access = True
                            break
                    if not has_access:
                        return abort(403)
            except Exception as ex:  # any kind of error(signature exp, invalid header, invalid payload and etc.)
                return abort(401)
            return f(*args, **kwargs)
        return wrapper
    return _login_required


def recaptcha(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if request.method == 'OPTIONS':
            return f(*args, **kwargs)
        json = request.get_json()
        if json:
            g_recaptcha_response = json.get('g_recaptcha_response')
            if g_recaptcha_response and validate_recaptcha(g_recaptcha_response):
                return f(*args, **kwargs)
        raise ForbiddenError('Your request could not be verified by recaptcha! Please try again')
    return wrapper


def validate_recaptcha(g_recaptcha_response):
    try:
        params = {
            'secret': current_config.RECAPTCHA_SECRET_KEY,
            'response': g_recaptcha_response,
            # 'remoteip': '',
        }
        response = requests.post('https://www.google.com/recaptcha/api/siteverify', data=params)
        if response.ok:
            return response.json().get('success', 'False')
    except Exception as ex:
        print('error on hsm validate_recaptcha request: ')
        print(ex)
    return False
