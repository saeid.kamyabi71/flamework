from functools import wraps

from flask import request, g


def pagination(default_page=1, default_per_page=30):
    def _paginate(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if request.method == 'OPTIONS':
                return f(*args, **kwargs)
            try:
                page = int(request.args.get('page'))
            except:
                page = default_page
            try:
                per_page = int(request.args.get('per_page'))
            except:
                per_page = default_per_page
            g.page = page
            g.per_page = per_page
            return f(*args, **kwargs)
        return wrapper
    return _paginate
