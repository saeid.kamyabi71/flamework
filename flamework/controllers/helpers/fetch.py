from functools import wraps

from flask import request, g


def add_request_option(**kwargs):
    if not hasattr(g, 'request_options'):
        g.request_options = {}
    g.request_options.update(kwargs)


def get_fetch_fields(allowed_fields):
    fetch_fields = set(request.args.get('fields', '').split(','))
    if allowed_fields:
        fetch_fields = allowed_fields.intersection(fetch_fields)
    return fetch_fields


def get_sort_fields(sort_fields):
    _fields = request.args.get('sort', '').split(',')
    _sort_fields = []
    ind = 10
    for field in _fields:
        field_name = field
        order = 1
        if field_name.startswith('-'):
            order = -1
            field_name = field_name[1:]
        if sort_fields and field_name not in sort_fields:
            continue
        item = (ind, field_name, order)
        _sort_fields.append(item)
        ind += 1
    return _sort_fields


def get_filter_fields():
    pass


def fields(allowed_fields=None):
    """fields to fetch
    :param allowed_fields:
    :type allowed_fields: set
    :return:
    """
    def _fields(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if request.method == 'OPTIONS':
                return f(*args, **kwargs)
            add_request_option(fields=get_fetch_fields(allowed_fields))
            return f(*args, **kwargs)
        return wrapper
    return _fields


def sortable(sort_fields=None):
    """sort by fields
    :param sort_fields:
    :type sort_fields: set
    :return:
    """
    def _sortable(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if request.method == 'OPTIONS':
                return f(*args, **kwargs)
            add_request_option(sort=get_sort_fields(sort_fields))
            return f(*args, **kwargs)
        return wrapper
    return _sortable
